<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <title>Detail Data</title>
</head>
<body>
    <h2>Detail Data Game</h2>
    <div class="card">
        <div class="card-header">
            <div class="card-title">Detail Game</div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-2">Nama</div>
                <div class="col-sm">{{ $data->name }}</div>
            </div>
            <div class="row">
                <div class="col-sm-2">Gameplay</div>
                <div class="col-sm">{{ $data->gameplay }}</div>
            </div>
            <div class="row">
                <div class="col-sm-2">Developer</div>
                <div class="col-sm">{{ $data->developer }}</div>
            </div>
            <div class="row">
                <div class="col-sm-2">Tahun</div>
                <div class="col-sm">{{ $data->year }}</div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>
</html>
