<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/welcome', 'AuthController@welcome')->name('welcome');

Route::get('/blank', function(){
    return view('layouts.master');
})->name('blank');

Route::get('/table', function(){
    return view('table.table');
})->name('table');

Route::get('/data-table', function(){
    return view('table.datatable');
})->name('data-table');

Route::resource('cast', CastController::class);
