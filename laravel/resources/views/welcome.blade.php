<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jabar Coding Camp</title>
</head>
<body>
    <h1>SELAMAT DATANG {{ $data['fname'] . ' ' . $data['lname'] }}</h1>
    <p><b>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</b></p>
</body>
</html>
