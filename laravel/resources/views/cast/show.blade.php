@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Cast</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Detail Cast</div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-2">Nama</div>
                    <div class="col-sm">{{ $data->nama }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Umur</div>
                    <div class="col-sm">{{ $data->umur }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Bio</div>
                    <div class="col-sm">{{ $data->bio }}</div>
                </div>
            </div>
        </div>
    </section>
@endsection
