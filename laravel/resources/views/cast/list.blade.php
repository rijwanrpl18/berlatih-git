@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Cast</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-sm btn-info float-sm-right" href="{{ route('cast.create') }}">
                        Tambah
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Bio</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $item)
                        <tr>
                            <td class="text-center">{{ $key + 1 }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->umur }}</td>
                            <td>{{ $item->bio }}</td>
                            <td class="text-center">
                                <a href="/cast/{{ $item->id }}" class="btn btn-info">Show</a>
                                <a href="/cast/{{ $item->id }}/edit" class="btn btn-primary">Edit</a>
                                <form action="/cast/{{ $item->id }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection
