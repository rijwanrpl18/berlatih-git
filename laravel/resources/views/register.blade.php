<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jabar Coding Camp</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <p><b>Sign Up Form</b></p>
    <form method="post" action="{{ route('welcome') }}">
        @csrf
        <label for="fname">First name :</label> <br>
        <input type="text" name="fname" id="fname" required> <br> <br>

        <label for="lname">Last name :</label> <br>
        <input type="text" name="lname" id="lname" required> <br> <br>

        <label for="gender">Gender</label> <br>
        <input type="radio" name="gender" id="gender"> Male <br>
        <input type="radio" name="gender" id="gender"> Female <br> <br>

        <label for="nationality">Nasionality</label> <br>
        <select name="nasionality" id="nasionality" required>
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Thailand</option>
        </select> <br><br>

        <label for="lang">Language Spoken</label> <br>
        <input type="checkbox" name="lang" id="indo"> Bahasa Indonesia <br>
        <input type="checkbox" name="lang" id="inggris"> English <br>
        <input type="checkbox" name="lang" id="other"> Other <br> <br>

        <label for="bio">Bio</label> <br>
        <textarea name="bio" id="bio" cols="30" rows="10" required></textarea> <br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
